package me.relevante.guesser.tracking;


import me.relevante.guesser.Provider;
import org.springframework.stereotype.Component;

@Component
public class LogRequestsTracker implements RequestTracker {

    private static final org.slf4j.Logger log = org.slf4j.LoggerFactory.getLogger(LogRequestsTracker.class);

    @Override
    public void track(Provider provider, String message) {
        log.info(provider.name() + " | " + message);
    }
}
