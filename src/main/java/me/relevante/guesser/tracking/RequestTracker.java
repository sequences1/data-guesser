package me.relevante.guesser.tracking;

import me.relevante.guesser.Provider;

public interface RequestTracker {

    void track(Provider provider, String message);

}
