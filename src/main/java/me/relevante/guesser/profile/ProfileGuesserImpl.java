package me.relevante.guesser.profile;

import me.relevante.guesser.Clues;
import me.relevante.guesser.Provider;
import me.relevante.guesser.profile.api.GuessedProfile;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component("mainProfileGuesser")
public class ProfileGuesserImpl implements ProfileGuesser {

    @Value("${profile-guesser.providers}")
    private String[] providers;

    @Resource
    private Map<Provider, ProfileGuesser> profileGuessers;

    @Override
    public Stream<GuessedProfile> guess(Clues clues) {
        for(String provider: providers) {
            ProfileGuesser profileGuesser = profileGuessers.get(Provider.valueOf(provider));
            List<GuessedProfile> guessedProfiles = profileGuesser.guess(clues).collect(Collectors.toList());
            if(!guessedProfiles.isEmpty()) return guessedProfiles.stream();
        }

        return Stream.empty();
    }
}
