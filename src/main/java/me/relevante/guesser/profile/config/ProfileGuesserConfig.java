package me.relevante.guesser.profile.config;

import com.google.common.collect.Maps;
import me.relevante.guesser.Provider;
import me.relevante.guesser.profile.ProfileGuesser;
import me.relevante.guesser.providers.fullContact.FullContactProfileGuesser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Configuration
public class ProfileGuesserConfig {

    @Bean
    public Map<Provider, ProfileGuesser> profileGuessers(FullContactProfileGuesser fullContactProfileGuesser) {
        Map<Provider, ProfileGuesser> profileGuessers = Maps.newHashMap();

        profileGuessers.put(Provider.FULL_CONTACT, fullContactProfileGuesser);

        return profileGuessers;
    }

}
