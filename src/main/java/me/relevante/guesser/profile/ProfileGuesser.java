package me.relevante.guesser.profile;

import me.relevante.guesser.Clues;
import me.relevante.guesser.profile.api.GuessedProfile;

import java.util.stream.Stream;

public interface ProfileGuesser {

    Stream<GuessedProfile> guess(Clues clues);

}
