package me.relevante.guesser;

public class Clues {

    private String name;
    private String surname;
    private String company;
    private String domain;
    private String email;

    private Clues(String name, String surname, String company, String domain, String email) {
        this.name = name;
        this.surname = surname;
        this.company = company;
        this.domain = domain;
        this.email = email;
    }

    public static Clues withDomain(String name, String surname, String domain) {
        return new Clues(name, surname, "", domain, "");
    }

    public static Clues withCompanyName(String name, String surname, String companyName) {
        return new Clues(name, surname, companyName, "", "");
    }

    public static Clues withEmail(String email) {
        return new Clues("", "", "", "", email);
    }

    public boolean hasDomain() {
        return domain != null && !domain.equals("");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Clues{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", company='" + company + '\'' +
                ", domain='" + domain + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
