package me.relevante.guesser.domain.config;

import com.google.common.collect.Maps;
import me.relevante.guesser.Provider;
import me.relevante.guesser.domain.DomainGuesser;
import me.relevante.guesser.providers.clearbit.ClearbitDomainGuesser;
import me.relevante.guesser.providers.fullContact.FullContactDomainGuesser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Configuration
public class DomainGuesserConfig {

    @Bean
    public Map<Provider, DomainGuesser> domainGuessers(ClearbitDomainGuesser clearbitDomainGuesser, FullContactDomainGuesser fullContactDomainGuesser) {
        Map<Provider, DomainGuesser> guessers = Maps.newHashMap();

        guessers.put(Provider.CLEARBIT, clearbitDomainGuesser);
        guessers.put(Provider.FULL_CONTACT, fullContactDomainGuesser);

        return guessers;
    }


}
