package me.relevante.guesser.domain;

import me.relevante.guesser.Clues;
import me.relevante.guesser.domain.api.GuessedDomain;

import java.util.stream.Stream;

public interface DomainGuesser {

    Stream<GuessedDomain> guess(Clues clues);

}
