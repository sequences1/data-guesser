package me.relevante.guesser.domain;

import me.relevante.guesser.Clues;
import me.relevante.guesser.Provider;
import me.relevante.guesser.domain.api.GuessedDomain;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component("mainDomainGuesser")
public class DomainGuesserImpl  implements DomainGuesser {

    @Resource
    private Map<Provider, DomainGuesser> domainGuessers;

    @Value("${domain-guesser.providers}")
    private String[] providers;

    @Override
    public Stream<GuessedDomain> guess(Clues clues) {

        for(String provider: providers) {
            DomainGuesser domainGuesser = domainGuessers.get(Provider.valueOf(provider));
            List<GuessedDomain> guessedDomains = domainGuesser.guess(clues).collect(Collectors.toList());
            if(!guessedDomains.isEmpty()) return guessedDomains.stream();
        }

        return Stream.empty();
    }

}
