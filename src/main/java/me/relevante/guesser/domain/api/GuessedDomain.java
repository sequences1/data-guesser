package me.relevante.guesser.domain.api;

public class GuessedDomain {

    private String domain;
    private Long score;

    public GuessedDomain(String domain, Long score) {
        this.domain = domain;
        this.score = score;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Long getScore() {
        return score;
    }

    public void setScore(Long score) {
        this.score = score;
    }
}
