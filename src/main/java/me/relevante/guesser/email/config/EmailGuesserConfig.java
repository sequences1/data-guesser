package me.relevante.guesser.email.config;

import com.google.common.collect.Maps;
import me.relevante.guesser.Provider;
import me.relevante.guesser.email.EmailGuesser;
import me.relevante.guesser.providers.voilaNorbert.VoilaNorbertEmailGuesser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Configuration
public class EmailGuesserConfig {

    @Bean
    public Map<Provider, EmailGuesser> emailGuessers(VoilaNorbertEmailGuesser voilaNorbertEmailGuesser) {
        Map<Provider, EmailGuesser> guessers = Maps.newHashMap();

        guessers.put(Provider.VOILA_NORBERT, voilaNorbertEmailGuesser);

        return guessers;
    }


}
