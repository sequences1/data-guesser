package me.relevante.guesser.email;

import me.relevante.guesser.Clues;
import me.relevante.guesser.Provider;
import me.relevante.guesser.domain.DomainGuesser;
import me.relevante.guesser.domain.api.GuessedDomain;
import me.relevante.guesser.email.api.GuessedEmail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component("mainEmailGuesser")
public class EmailGuesserImpl implements EmailGuesser {

    @Value("${email-guesser.providers}")
    private String[] providers;

    @Resource
    private Map<Provider, EmailGuesser> emailGuessers;

    @Autowired
    @Qualifier("mainDomainGuesser")
    private DomainGuesser domainGuesser;

    @Override
    public Stream<GuessedEmail> guess(Clues clues) {

        Stream<GuessedDomain> domains = clues.hasDomain() ? Stream.of(new GuessedDomain(clues.getDomain(), 1L)) : domainGuesser.guess(clues);

        for(String provider: providers) {
            List<GuessedEmail> guessedEmails = domains
                    .map(guessedDomain -> Clues.withDomain(clues.getName(), clues.getSurname(), guessedDomain.getDomain()))
                    .flatMap(domain -> emailGuessers.get(Provider.valueOf(provider)).guess(domain))
                    .collect(Collectors.toList());
            if(!guessedEmails.isEmpty()) return guessedEmails.stream();
        }

        return Stream.empty();
    }

}
