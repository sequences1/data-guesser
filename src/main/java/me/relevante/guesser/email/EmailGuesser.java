package me.relevante.guesser.email;

import me.relevante.guesser.Clues;
import me.relevante.guesser.domain.api.GuessedDomain;
import me.relevante.guesser.email.api.GuessedEmail;

import java.util.stream.Stream;

public interface EmailGuesser {

    Stream<GuessedEmail> guess(Clues clues);

}
