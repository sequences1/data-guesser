package me.relevante.guesser.email.api;

public class GuessedEmail {
    private String email;
    private Integer score;

    public GuessedEmail(String email, Integer score) {
        this.email = email;
        this.score = score;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }
}
