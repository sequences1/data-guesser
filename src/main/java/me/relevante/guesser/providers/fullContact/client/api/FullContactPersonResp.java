
package me.relevante.guesser.providers.fullContact.client.api;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "status",
    "requestId",
    "likelihood",
    "photos",
    "contactInfo",
    "organizations",
    "demographics",
    "socialProfiles",
    "digitalFootprint"
})
public class FullContactPersonResp {

    @JsonProperty("status")
    private Integer status;
    @JsonProperty("requestId")
    private String requestId;
    @JsonProperty("likelihood")
    private Double likelihood;
    @JsonProperty("photos")
    private List<Photo> photos = null;
    @JsonProperty("contactInfo")
    private ContactInfo contactInfo;
    @JsonProperty("organizations")
    private List<Organization> organizations = null;
    @JsonProperty("demographics")
    private Demographics demographics;
    @JsonProperty("socialProfiles")
    private List<SocialProfile> socialProfiles = null;
    @JsonProperty("digitalFootprint")
    private DigitalFootprint digitalFootprint;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("status")
    public Integer getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(Integer status) {
        this.status = status;
    }

    @JsonProperty("requestId")
    public String getRequestId() {
        return requestId;
    }

    @JsonProperty("requestId")
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @JsonProperty("likelihood")
    public Double getLikelihood() {
        return likelihood;
    }

    @JsonProperty("likelihood")
    public void setLikelihood(Double likelihood) {
        this.likelihood = likelihood;
    }

    @JsonProperty("photos")
    public List<Photo> getPhotos() {
        return photos;
    }

    @JsonProperty("photos")
    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    @JsonProperty("contactInfo")
    public ContactInfo getContactInfo() {
        return contactInfo;
    }

    @JsonProperty("contactInfo")
    public void setContactInfo(ContactInfo contactInfo) {
        this.contactInfo = contactInfo;
    }

    @JsonProperty("organizations")
    public List<Organization> getOrganizations() {
        return organizations;
    }

    @JsonProperty("organizations")
    public void setOrganizations(List<Organization> organizations) {
        this.organizations = organizations;
    }

    @JsonProperty("demographics")
    public Demographics getDemographics() {
        return demographics;
    }

    @JsonProperty("demographics")
    public void setDemographics(Demographics demographics) {
        this.demographics = demographics;
    }

    @JsonProperty("socialProfiles")
    public List<SocialProfile> getSocialProfiles() {
        return socialProfiles;
    }

    @JsonProperty("socialProfiles")
    public void setSocialProfiles(List<SocialProfile> socialProfiles) {
        this.socialProfiles = socialProfiles;
    }

    @JsonProperty("digitalFootprint")
    public DigitalFootprint getDigitalFootprint() {
        return digitalFootprint;
    }

    @JsonProperty("digitalFootprint")
    public void setDigitalFootprint(DigitalFootprint digitalFootprint) {
        this.digitalFootprint = digitalFootprint;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
