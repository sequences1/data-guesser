package me.relevante.guesser.providers.fullContact;

import me.relevante.guesser.Clues;
import me.relevante.guesser.Provider;
import me.relevante.guesser.domain.DomainGuesser;
import me.relevante.guesser.domain.api.GuessedDomain;
import me.relevante.guesser.providers.fullContact.client.FullContactClient;
import me.relevante.guesser.tracking.RequestTracker;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.stream.Stream;

@Component
public class FullContactDomainGuesser implements DomainGuesser {

    @Inject
    private FullContactClient fullContactClient;

    @Inject
    private RequestTracker requestTracker;

    @Override
    public Stream<GuessedDomain> guess(Clues clues) {
        requestTracker.track(Provider.FULL_CONTACT, "Started search for clues: " + clues);
        return fullContactClient.getCompanyDataByName(clues.getCompany()).stream()
                .map(domain -> new GuessedDomain(domain.getLookupDomain(), 0L));
    }
}
