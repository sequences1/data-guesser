package me.relevante.guesser.providers.fullContact;

import me.relevante.guesser.Clues;
import me.relevante.guesser.Provider;
import me.relevante.guesser.profile.api.GuessedProfile;
import me.relevante.guesser.profile.ProfileGuesser;
import me.relevante.guesser.providers.fullContact.client.FullContactClient;
import me.relevante.guesser.tracking.RequestTracker;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.stream.Stream;

@Component
public class FullContactProfileGuesser implements ProfileGuesser {

    private FullContactClient fullContactClient;
    private RequestTracker requestTracker;

    @Inject
    public FullContactProfileGuesser(FullContactClient fullContactClient, RequestTracker requestTracker) {
        this.fullContactClient = fullContactClient;
        this.requestTracker = requestTracker;
    }

    @Override
    public Stream<GuessedProfile> guess(Clues clues) {
        requestTracker.track(Provider.FULL_CONTACT, "Started search for clues " + clues);
        return fullContactClient.getAllDataFromEmail(clues.getEmail()).getSocialProfiles().stream()
                .map(p -> new GuessedProfile(p.getType(), p.getUrl()));
    }
}
