
package me.relevante.guesser.providers.fullContact.client.api;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "locationDeduced",
    "gender",
    "locationGeneral"
})
public class Demographics {

    @JsonProperty("locationDeduced")
    private LocationDeduced locationDeduced;
    @JsonProperty("gender")
    private String gender;
    @JsonProperty("locationGeneral")
    private String locationGeneral;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("locationDeduced")
    public LocationDeduced getLocationDeduced() {
        return locationDeduced;
    }

    @JsonProperty("locationDeduced")
    public void setLocationDeduced(LocationDeduced locationDeduced) {
        this.locationDeduced = locationDeduced;
    }

    @JsonProperty("gender")
    public String getGender() {
        return gender;
    }

    @JsonProperty("gender")
    public void setGender(String gender) {
        this.gender = gender;
    }

    @JsonProperty("locationGeneral")
    public String getLocationGeneral() {
        return locationGeneral;
    }

    @JsonProperty("locationGeneral")
    public void setLocationGeneral(String locationGeneral) {
        this.locationGeneral = locationGeneral;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
