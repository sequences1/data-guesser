
package me.relevante.guesser.providers.fullContact.client.api;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "normalizedLocation",
    "deducedLocation",
    "city",
    "state",
    "country",
    "continent",
    "county",
    "likelihood"
})
public class LocationDeduced {

    @JsonProperty("normalizedLocation")
    private String normalizedLocation;
    @JsonProperty("deducedLocation")
    private String deducedLocation;
    @JsonProperty("city")
    private City city;
    @JsonProperty("state")
    private State state;
    @JsonProperty("country")
    private Country country;
    @JsonProperty("continent")
    private Continent continent;
    @JsonProperty("county")
    private County county;
    @JsonProperty("likelihood")
    private Integer likelihood;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("normalizedLocation")
    public String getNormalizedLocation() {
        return normalizedLocation;
    }

    @JsonProperty("normalizedLocation")
    public void setNormalizedLocation(String normalizedLocation) {
        this.normalizedLocation = normalizedLocation;
    }

    @JsonProperty("deducedLocation")
    public String getDeducedLocation() {
        return deducedLocation;
    }

    @JsonProperty("deducedLocation")
    public void setDeducedLocation(String deducedLocation) {
        this.deducedLocation = deducedLocation;
    }

    @JsonProperty("city")
    public City getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(City city) {
        this.city = city;
    }

    @JsonProperty("state")
    public State getState() {
        return state;
    }

    @JsonProperty("state")
    public void setState(State state) {
        this.state = state;
    }

    @JsonProperty("country")
    public Country getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(Country country) {
        this.country = country;
    }

    @JsonProperty("continent")
    public Continent getContinent() {
        return continent;
    }

    @JsonProperty("continent")
    public void setContinent(Continent continent) {
        this.continent = continent;
    }

    @JsonProperty("county")
    public County getCounty() {
        return county;
    }

    @JsonProperty("county")
    public void setCounty(County county) {
        this.county = county;
    }

    @JsonProperty("likelihood")
    public Integer getLikelihood() {
        return likelihood;
    }

    @JsonProperty("likelihood")
    public void setLikelihood(Integer likelihood) {
        this.likelihood = likelihood;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
