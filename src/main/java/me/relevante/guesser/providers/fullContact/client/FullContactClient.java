package me.relevante.guesser.providers.fullContact.client;

import me.relevante.guesser.providers.fullContact.client.api.FullContactCompanyNameResp;
import me.relevante.guesser.providers.fullContact.client.api.FullContactPersonResp;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.*;

@FeignClient(name = "full-contact", url = "${full-contact.url}", configuration = FullContactClientConfig.class)
public interface FullContactClient {

    @RequestMapping(method = RequestMethod.GET, value = "/person.json", params = {"email"})
    FullContactPersonResp getAllDataFromEmail(@RequestParam("email") String email);

    @RequestMapping(method = RequestMethod.GET, value = "/company/search.json", params = {"companyName"})
    List<FullContactCompanyNameResp> getCompanyDataByName(@RequestParam("companyName") String companyName);

}
