package me.relevante.guesser.providers.anyMailFinder.client.api;

public class AnyMailFinderReq {

    private String domain;
    private String name;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
