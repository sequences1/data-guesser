package me.relevante.guesser.providers.anyMailFinder.client;

import me.relevante.guesser.providers.anyMailFinder.client.api.AnyMailFinderReq;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "any-mail-finder", url = "${any-mail-finder.url}", configuration = AnyMailFinderClientConfig.class)
public interface AnyMailFinderClient {

    @RequestMapping(method = RequestMethod.POST, value = "/search/person.json", consumes = MediaType.APPLICATION_JSON_VALUE)
    String searchForEmail(@RequestBody AnyMailFinderReq req);

}
