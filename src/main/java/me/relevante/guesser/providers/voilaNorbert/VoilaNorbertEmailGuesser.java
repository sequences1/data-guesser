package me.relevante.guesser.providers.voilaNorbert;


import me.relevante.guesser.Clues;
import me.relevante.guesser.Provider;
import me.relevante.guesser.domain.DomainGuesser;
import me.relevante.guesser.domain.api.GuessedDomain;
import me.relevante.guesser.email.EmailGuesser;
import me.relevante.guesser.email.api.GuessedEmail;
import me.relevante.guesser.providers.voilaNorbert.client.VoilaNorbertClient;
import me.relevante.guesser.providers.voilaNorbert.client.api.VoilaNorbertSearchForEmailResp;
import me.relevante.guesser.tracking.RequestTracker;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;

import javax.inject.Inject;
import java.util.stream.Stream;

@Component
public class VoilaNorbertEmailGuesser implements EmailGuesser {

    @Inject
    private VoilaNorbertClient voilaNorbertClient;

    @Inject
    private RequestTracker requestTracker;

    @Inject
    @Qualifier("mainDomainGuesser")
    private DomainGuesser domainGuesser;

    @Override
    public Stream<GuessedEmail> guess(Clues clues) {
        VoilaNorbertSearchForEmailResp resp = getVoilaNorbertSearchForEmailResp(clues);
        return Stream.of(new GuessedEmail(resp.getEmail().getEmail(), resp.getEmail().getScore()));
    }

    private VoilaNorbertSearchForEmailResp getVoilaNorbertSearchForEmailResp(Clues clues) {
        VoilaNorbertSearchForEmailResp resp = voilaNorbertClient.searchForEmail(buildParams(clues));
        if(resp.getSearching()) {
            requestTracker.track(Provider.VOILA_NORBERT, "Started search for clues: " + clues);
            return getVoilaNorbertSearchForEmailResp(clues);
        }
        return resp;
    }

    private String buildFullName(Clues clues) {
        return clues.getName() + " " + clues.getSurname();
    }

    private LinkedMultiValueMap<String, String> buildParams(Clues clues) {
        LinkedMultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.set("name", buildFullName(clues));
        params.set("domain", clues.getDomain());
        return params;
    }

}
