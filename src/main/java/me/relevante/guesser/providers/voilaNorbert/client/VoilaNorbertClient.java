package me.relevante.guesser.providers.voilaNorbert.client;

import me.relevante.guesser.providers.voilaNorbert.client.api.VoilaNorbertSearchForEmailResp;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

@FeignClient(name = "voila-norbert", url = "${voila-norbert.url}", configuration = VoilaNorbertClientConfig.class)
public interface VoilaNorbertClient {

    @RequestMapping(method = RequestMethod.POST, value = "/search/name")
    VoilaNorbertSearchForEmailResp searchForEmail(MultiValueMap map);

}
