
package me.relevante.guesser.providers.voilaNorbert.client.api;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "company",
    "created",
    "email",
    "id",
    "lists",
    "name",
    "owner",
    "searching",
    "status"
})
public class VoilaNorbertSearchForEmailResp {

    @JsonProperty("company")
    private Company company;
    @JsonProperty("created")
    private Long created;
    @JsonProperty("email")
    private Email email;
    @JsonProperty("id")
    private Long id;
    @JsonProperty("lists")
    private List<Object> lists = null;
    @JsonProperty("name")
    private String name;
    @JsonProperty("owner")
    private Owner owner;
    @JsonProperty("searching")
    private Boolean searching;
    @JsonProperty("status")
    private String status;

    @JsonProperty("company")
    public Company getCompany() {
        return company;
    }

    @JsonProperty("company")
    public void setCompany(Company company) {
        this.company = company;
    }

    @JsonProperty("created")
    public Long getCreated() {
        return created;
    }

    @JsonProperty("created")
    public void setCreated(Long created) {
        this.created = created;
    }

    @JsonProperty("email")
    public Email getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(Email email) {
        this.email = email;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("lists")
    public List<Object> getLists() {
        return lists;
    }

    @JsonProperty("lists")
    public void setLists(List<Object> lists) {
        this.lists = lists;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("owner")
    public Owner getOwner() {
        return owner;
    }

    @JsonProperty("owner")
    public void setOwner(Owner owner) {
        this.owner = owner;
    }

    @JsonProperty("searching")
    public Boolean getSearching() {
        return searching;
    }

    @JsonProperty("searching")
    public void setSearching(Boolean searching) {
        this.searching = searching;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

}
