package me.relevante.guesser.providers.clearbit;

import me.relevante.guesser.Clues;
import me.relevante.guesser.domain.DomainGuesser;
import me.relevante.guesser.domain.api.GuessedDomain;
import me.relevante.guesser.providers.clearbit.client.ClearbitClient;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.stream.Stream;

@Component
public class ClearbitDomainGuesser implements DomainGuesser {

    private final ClearbitClient clearbitClient;

    @Inject
    public ClearbitDomainGuesser(ClearbitClient clearbitClient) {
        this.clearbitClient = clearbitClient;
    }

    @Override
    public Stream<GuessedDomain> guess(Clues clues) {
        return clearbitClient.getCompanyAutoComplete(clues.getCompany()).stream()
                .map(domain -> new GuessedDomain(domain.getDomain(), 0L));
    }

}
