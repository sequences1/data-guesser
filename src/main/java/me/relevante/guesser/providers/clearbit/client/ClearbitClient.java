package me.relevante.guesser.providers.clearbit.client;

import me.relevante.guesser.providers.clearbit.client.api.ClearbitResp;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "clearbit", url = "${clearbit.url}", configuration = ClearbitClientConfig.class)
public interface ClearbitClient {

    @RequestMapping(method = RequestMethod.GET, value = "/companies/suggest", params = {"query"})
    List<ClearbitResp> getCompanyAutoComplete(@RequestParam("query") String query);

}
