package me.relevante.guesser;

public enum Provider {
    CLEARBIT, FULL_CONTACT, VOILA_NORBERT, ANY_MAIL_FINDER
}
