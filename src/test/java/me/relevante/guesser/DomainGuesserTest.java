package me.relevante.guesser;

import me.relevante.guesser.domain.DomainGuesser;
import me.relevante.guesser.domain.api.GuessedDomain;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.inject.Inject;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


public class DomainGuesserTest extends BaseTest {

    @Inject
    @Qualifier("mainDomainGuesser")
    private DomainGuesser domainGuesser;

    // "fintonic.com", "fintonic.cl"
    // "paradigmadigital.com", "paradigmatecnologico.com", "paradigma.com.tr", "en.paradigmadigital.com"
    // "ibermatica.com", "ibermaticasb.com", "ibermatica.es"

    private static Collection<Object[]> testData =
        Arrays.asList(new Object[][] {
                { Clues.withCompanyName("NotUsed", "NotUsed", "Relevante.me"), new GuessedDomain("relevante.me", 0L) },
                { Clues.withCompanyName("NotUsed", "NotUsed", "Fintonic"), new GuessedDomain("fintonic.com", 0L), },
                { Clues.withCompanyName("NotUsed", "NotUsed", "Eurobits"), new GuessedDomain("eurobits.es", 0L), },
                { Clues.withCompanyName("NotUsed", "NotUsed", "Paradigma Digital"), new GuessedDomain("paradigmadigital.com", 0L), },
                { Clues.withCompanyName("NotUsed", "NotUsed", "Ibermática"), new GuessedDomain("ibermatica.com", 0L), },
                { Clues.withCompanyName("NotUsed", "NotUsed", "Mooverang"), new GuessedDomain("mooverang.es", 0L), },
                { Clues.withCompanyName("NotUsed", "NotUsed", "eSynergy Solutions"), new GuessedDomain("esynergy-solutions.co.uk", 0L), }
        });

    @Test
    public void shouldGuessAllTheseDomains() {
        testData.forEach(data -> {
            List<GuessedDomain> guessed = domainGuesser.guess((Clues) data[0]).collect(Collectors.toList());

            assertThat("The number of guessed domains is not the expected", guessed, hasSize(1));
            assertThat("The guessed domain is not the expected", guessed.get(0), samePropertyValuesAs(data[1]));
        });
    }

}
